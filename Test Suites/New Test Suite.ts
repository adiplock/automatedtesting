<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-05T10:49:34</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ec4dd2e0-b243-4283-a750-90a46f59550c</testSuiteGuid>
   <testCaseLink>
      <guid>45735327-4621-4711-96c2-28d81cf9ecc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/13- Theme Library page appears and theme loads</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>954cea3b-e655-4892-9e67-5117f7e46891</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14 - Global Scheme page appears with Scheme Template</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6391e5d2-8158-4bbc-808c-b7144f628015</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T10 - Users page appears and user data loads</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b7edb00-917c-4f15-af4c-c3f3dc38e90f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC1 - Can Login with valid credential</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4036bc14-ebb2-47af-b342-015d2c5825fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC11 - When Benefits link is clicked, and a Partner is selected Manage Benefits page appears</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca3955de-d90d-4e47-add5-2292c60f378e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC12 - Provider page appears and provider data loads</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6509a4c7-1155-4d55-a679-4a7dfee4cd50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC2 - Can login successfully with valid credential</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d30f37b6-d8fa-491d-afed-ba81a81d4313</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC3 - Logout link on top right-hand side drop-down list is active</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91b61273-ae34-465f-92c8-2b54235d3fd4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC4 - Logout link on left-hand side menu bar is active</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7431bf95-a449-4829-9497-c51128d72b12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC5 - All 8 links text are present on the page-content area</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bef96423-9950-48a3-a5c7-60f0167a4e7f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cdd7deaf-4d67-4054-9a72-d911395c3bbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC6 - All 8 links are active on page-content area and taking the user to the correct page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>142f3c7a-dd7e-4ab6-be11-bfa67560fa02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC7 - User can download Audit Report</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88947ce3-eaf9-4e66-8397-7a82776dbd4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC8 - Partner page appears and partner data loads</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7103359-1043-48fc-859a-6089f4bbbe7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC9 - Clients page appears and client data loads</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
